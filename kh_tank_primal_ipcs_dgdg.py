# encoding: utf8
from __future__ import division
import time, traceback, sys
import dolfin as df
from dolfin import grad, div, dot, dx, ds, dS, cos, sin, jump, avg
from kh_tank_helpers import SlopeLimiter, VelocityProjectionPrimal, SolutionProperties 
from kh_tank_helpers import HydrostaticPressure, Input, total_energy, define_penalty
from orangutan import XDMFSaver, Log, should_stop, save_restart_file, load_restart_file, NCPUS, PyCode


###############################################################################
# Input data

inpfile = sys.argv[1]
print 'Reading input file %s' % inpfile
inp = Input()
Input.space_rho = 'DG'
Input.order_rho = 1
Input.space_u = 'DG'
Input.order_u = 2
Input.space_p = 'DG'
Input.order_p = 1
Input.p_initial = '0'
inp.read(inpfile)


###############################################################################
# Preliminaries

# Setup logging
log = Log(inp.output_file)
df.set_log_level(df.WARNING)
log.info('Running KH Tank with input:\n')
log.info(str(inp))
log.info('---------\n')

# Mesh and function spaces
df.parameters['ghost_mode'] = 'shared_vertex'
p0 = df.Point(-inp.length/2, -inp.height/2)
p1 = df.Point(inp.length/2, inp.height/2)
mesh = df.RectangleMesh(p0, p1, inp.Nx, inp.Ny, inp.diagonal)
Vr = df.FunctionSpace(mesh, inp.space_rho, inp.order_rho)
Vu = df.VectorFunctionSpace(mesh, inp.space_u, inp.order_u)
Vp = df.FunctionSpace(mesh, inp.space_p, inp.order_p)

# Current time step
rho_h = df.Function(Vr)
u_h = df.Function(Vu)
p_h = df.Function(Vp)

# Previous time step
rho_p = df.Function(Vr)
u_p = df.Function(Vu)
p_p = df.Function(Vp)

# Give names for output file
for func, name in [(rho_h, 'rho'), (rho_p, 'rho_p'),
                   (u_h, 'u'), (u_p, 'u_p'),
                   (p_h, 'p'), (p_p, 'p_p')]:
    func.rename(name, name)

# Initial condition
el = Vr.ufl_element()
params = dict(rho0=inp.rho0, drho=inp.drho, Y0=inp.Y0, Ad=inp.Ad, Ld=inp.Ld,
              h=inp.height, b=inp.length, g=inp.g)
rho_expr = df.Expression(inp.rho_initial, element=el, **params)
rho_p.assign(df.interpolate(rho_expr, Vr))
rho_h.assign(rho_p)
rho_min = df.MPI.min(df.mpi_comm_world(), rho_h.vector().min())
rho_max = df.MPI.max(df.mpi_comm_world(), rho_h.vector().max())
df_rho_min = df.Constant(rho_min)
log.info('Density range: %.2f - %.2f\n' % (rho_min, rho_max))

# Slope limiter
if inp.slope_limiter_rho != 'none':
    slope_limiter_rho = SlopeLimiter(rho_h, inp.slope_limiter_rho)
    sle_rho = slope_limiter_rho.excedance
    sle_rho.rename('sleRho', 'Slope limiter exceedance for rho')

# Velocity projection
velproj = VelocityProjectionPrimal(u_h)

# Dolfin constants and functions
df_g = df.Constant(inp.g)
df_dt = df.Constant(inp.dt)
df_rho0 = df.Constant(inp.rho0)
df_mu = df.Constant(inp.nu)*rho_h
n = df.FacetNormal(mesh)
h = df.CellSize(mesh)
x = df.SpatialCoordinate(mesh)
zero = df.Constant(0.0)

# Tank motion
code_w0 = PyCode(inp.w0, 'input w0')
df_w0 = df.Constant(0.0)
gvec = df.as_vector([df_g*sin(df_w0), -df_g*cos(df_w0)])
def move_tank(t, it):
    df_w0.assign(code_w0.run(t=t, it=it, inp=inp))
move_tank(0.0, 0)

# Hydrostatic pressure
#hydp = HydrostaticPressure(rho_h, gvec, Vp)
#hydp.update()
#p_h.assign(hydp.p_hydrostatic)
#p_p.assign(hydp.p_hydrostatic)

# Starting values
mass0 = df.assemble(rho_p*dx(mesh))
x_e = df.as_vector([x[0], x[1]+df.Constant(inp.height/2)])
Ek0, Ep0 = total_energy(rho_p, u_h, gvec, x_e)
log.info('Starting with mass %.15g\n' % mass0)
log.info('Starting with kinetic energy   %15.5e\n' % Ek0)
log.info('Starting with potential energy %15.5e\n' % Ep0)


###############################################################################
# Weak forms

rho, vr = df.TrialFunction(Vr), df.TestFunction(Vr)
u, v = df.TrialFunction(Vu), df.TestFunction(Vu)
p, q = df.TrialFunction(Vp), df.TestFunction(Vp)

penalty_u_dS, penalty_u_ds = define_penalty(mesh, inp.order_u, inp.nu*rho_min, inp.nu*rho_max, log=log)
penalty_p_dS, penalty_p_ds = define_penalty(mesh, inp.order_u, 1/rho_max, 1/rho_min, log=log)
D12 = df.as_vector([0, 0])
                   
# Upwind and downwind velocities
u_conv = u_p
w_nU = (dot(u_conv, n) + abs(dot(u_conv, n)))/2.0
w_nD = (dot(u_conv, n) - abs(dot(u_conv, n)))/2.0

use_grad_p_form = False
c1, c2 = 1, -1

# Transport equation for rho
eq1 = (c1*rho + c2*rho_p)/df_dt*vr*dx
eq1 -= rho*dot(u_h, grad(vr))*dx
eq1 += jump(rho*w_nU)*jump(vr)*dS
#eq1 += rho/2*div(u_conv)*vr*dx # skew symmetric convection
a1, L1 = df.system(eq1)

# Momentum equation for the tentative velocity
eq2 = 0
for d in range(2):
    # Time derivative
    # ∂(ρu)/∂t
    eq2 += rho_h*(c1*u[d] + c2*u_p[d])/df_dt*v[d]*dx
    
    # Convection:
    # w⋅∇(ρu)    
    flux_nU = u[d]*w_nU
    flux = jump(flux_nU)
    eq2 -= rho_h*u[d]*div(v[d]*u_conv)*dx
    eq2 += flux*jump(rho_h*v[d])*dS
    
    # Diffusion:
    # -∇⋅∇u
    eq2 += df_mu*dot(grad(u[d]), grad(v[d]))*dx
    
    # Symmetric Interior Penalty method for -∇⋅μ∇u
    eq2 -= avg(df_mu)*dot(n('+'), avg(grad(u[d])))*jump(v[d])*dS
    eq2 -= avg(df_mu)*dot(n('+'), avg(grad(v[d])))*jump(u[d])*dS
    
    # Symmetric Interior Penalty coercivity term
    eq2 += penalty_u_dS*jump(u[d])*jump(v[d])*dS
    
    # Pressure
    # ∇p
    if use_grad_p_form:
        eq2 += v[d]*p_p.dx(d)*dx
        eq2 -= (avg(v[d]) + dot(D12, jump(v[d], n)))*jump(p_p)*n[d]('+')*dS
    else:
        eq2 -= p_p*v[d].dx(d)*dx
        eq2 += (avg(p_p) - dot(D12, jump(p_p, n)))*jump(v[d])*n[d]('+')*dS
    
    # Body force (gravity)
    # ρ g
    eq2 -= rho_h*gvec[d]*v[d]*dx
    
    # Dirichlet boundary
    for dds, u_bc in ((ds, zero), ):
        # Convection
        eq2 += rho_h*u[d]*w_nU*v[d]*dds
        eq2 += rho_h*u_bc*w_nD*v[d]*dds
        
        # SIPG for -∇⋅μ∇u
        eq2 -= df_mu*dot(n, grad(u[d]))*v[d]*dds
        eq2 -= df_mu*dot(n, grad(v[d]))*u[d]*dds
        eq2 += df_mu*dot(n, grad(v[d]))*u_bc*dds
        
        # Weak Dirichlet
        eq2 += penalty_u_ds*u[d]*v[d]*dds
        eq2 -= penalty_u_ds*u_bc*v[d]*dds
        
        # Pressure
        if not use_grad_p_form:
            eq2 += p_p*v[d]*n[d]*dds
    
    # Neumann boundary
    for nds, du_bc in ():
        eq2 += df_mu*du_bc*v[d]*nds
        
        if not use_grad_p_form:
            eq2 += p_p*v[d]*n[d]*nds
a2, L2 = df.system(eq2)

# Pressure correction equation
# Weak form of the Poisson eq. with discontinuous elements
# -∇⋅(1/ρ ∇p) = - γ_1/Δt ∇⋅u^*
K = 1.0/rho_h
eq3 = K*dot(grad(p - p_p), grad(q))*dx

# RHS, ∇⋅u^*
u_flux = avg(u_h)
eq3 -= c1/df_dt*dot(u_h, grad(q))*dx
eq3 += c1/df_dt*dot(u_flux, n('+'))*jump(q)*dS
#eq3 += c1/df_dt*dot(u_h, n)*q*ds

# Symmetric Interior Penalty method for -∇⋅∇p
eq3 -= dot(n('+'), avg(K*grad(p)))*jump(q)*dS
eq3 -= dot(n('+'), avg(K*grad(q)))*jump(p)*dS

# Symmetric Interior Penalty method for -∇⋅∇p^*
eq3 += dot(n('+'), avg(K*grad(p_p)))*jump(q)*dS
eq3 += dot(n('+'), avg(K*grad(q)))*jump(p_p)*dS

# Symmetric Interior Penalty coercivity term
eq3 += penalty_p_dS*jump(p)*jump(q)*dS
eq3 -= penalty_p_dS*jump(p_p)*jump(q)*dS

# Dirichlet boundary
for dds, p_bc in ():
    # From integration by parts of RHS
    #u_bc=zero: eq3 += c1/df_dt*dot(u_bc, n)*q*dds
    
    # SIPG for -∇⋅∇p
    eq3 -= dot(n, K*grad(p))*q*dds
    eq3 -= dot(n, K*grad(q))*p*dds
    eq3 += dot(n, K*grad(q))*p_bc*dds
    
    # SIPG for -∇⋅∇p^*
    eq3 += dot(n, K*grad(p_p))*q*dds
    eq3 += dot(n, K*grad(q))*p_p*dds
    
    # Weak Dirichlet
    eq3 += penalty_p_ds*p*q*dds
    eq3 -= penalty_p_ds*p_bc*q*dds
    
    # Weak Dirichlet for p^*
    eq3 -= penalty_p_ds*p_p*q*dds
    eq3 += penalty_p_ds*p_bc*q*dds

# Neumann boundary conditions
for nds, dp_bc in ((ds, zero), ):
    # Neumann boundary conditions on p and p_star cancel
    #eq3 -= (dp_bc - dot(n, grad(p_p)))*q*nds

    # From integration by parts of RHS
    #u_bc=zero: eq3 += c1/df_dt*dot(u_bc, n)*q*nds
    pass
a3, L3 = df.system(eq3)


# Velocity update
a4 = dot(u, v)*dx
L4 = dot(u_h, v)*dx - df_dt/rho_h*dot(grad(p_p - p_h), v)*dx


###############################################################################
# Boundary conditions

all_boundaries = lambda x, on_boundary: on_boundary
#bcs = [df.DirichletBC(W.sub(1), zero, all_boundaries),
#       df.DirichletBC(W.sub(2), zero, all_boundaries)]
bcs = []


###############################################################################
# Linear equation solvers

solver1 = df.KrylovSolver('gmres', 'default')
solver2 = df.KrylovSolver('gmres', 'default')
solver3 = df.KrylovSolver('gmres', 'hypre_amg')

for solver in (solver1, solver2, solver3):
    solver.parameters['nonzero_initial_guess'] = True
    solver.parameters['relative_tolerance'] = 1e-12
    solver.parameters['absolute_tolerance'] = 1e-12
    solver.parameters['preconditioner']['structure'] = 'same_nonzero_pattern'

solver4 = df.LocalSolver(a4, L4)
solver4.factorize()


###############################################################################
# Run simulation

# Calculate solution properties
solprops = SolutionProperties(mesh, u_h, df_dt, inp.nu, divergence='div')
solprops._div_dS.rename('div_dS', 'div_dS')
solprops._div_dx.rename('div_dx', 'div_dx')

# Prepare XDMF output file to store results for visualisation
xdmf_funcs = [rho_h, u_h, p_h, solprops._div_dS, solprops._div_dx]
if inp.slope_limiter_rho != 'none':
    xdmf_funcs.append(slope_limiter_rho.excedance)
xdmf = XDMFSaver(xdmf_funcs, 'results')

# Functions needed to restart
restartfuncs = [rho_p, u_p, p_h]

# Create null space vector in Vp Space
null_func = df.Function(Vp)
null_vec = null_func.vector()
null_vec[:] = 1
null_vec *= 1/null_vec.norm("l2")
pressure_null_space = df.VectorSpaceBasis([null_func.vector()])

def time_loop():
    t = 0.0
    it = 0
    xdmf.save(t)
    
    while t <= inp.tmax - inp.dt + 1e-6:
        it += 1
        t += inp.dt
        t0_all = time.time()
        
        # Restart from file
        if it == 1 and inp.restart_from_file:
            log.info('Loading restart file %r\n' % inp.restart_from_file)
            t, it = load_restart_file(inp.restart_from_file, restartfuncs, log)
        
        log.report('Time', t, '%7.3f')
        move_tank(t, it)
        
        with log.timer('rho'):
            A1, b1 = df.assemble_system(a1, L1)
            solver1.solve(A1, rho_h.vector(), b1)
            if inp.slope_limiter_rho != 'none':
                slope_limiter_rho.run()
        
        for inner_iter in range(100):
            with log.timer('u'):
                A2, b2 = df.assemble_system(a2, L2)
                solver2.solve(A2, u_h.vector(), b2)
            
            with log.timer('p'):
                A3, b3 = df.assemble_system(a3, L3)
                
                # Make sure the null space is set on the matrix
                df.as_backend_type(A3).set_nullspace(pressure_null_space)
    
                # Orthogonalize b with respect to the null space
                pressure_null_space.orthogonalize(b3)
                
                solver3.solve(A3, p_h.vector(), b3)
                
                # Removing the null space of the matrix system is not strictly the same as removing
                # the null space of the equation, so we correct for this here
                vol = df.assemble(df.Constant(1)*dx(domain=mesh))
                p_avg = df.assemble(p_h*dx(domain=mesh))/vol
                p_h.vector()[:] -= p_avg
            
            # Advance to the next inner iteration
            p_p.vector().axpy(-1, p_h.vector())
            pnorm = df.norm(p_p.vector(), 'l2') / (df.norm(p_h.vector(), 'l2') + 1e-8) 
            print 'Inner iter %d, pnorm %10.2e' % (inner_iter+1, pnorm)
            p_p.assign(p_h)
            
            if pnorm < 1e-8:
                break
        
        # Velocity update and BDM projection
        #solver4.solve_local_rhs(u_h)
        velproj.run()
        u_h.assign(velproj.velocity)
        
        # Advance in time
        rho_p.assign(rho_h)
        u_p.assign(u_h)
        
        with log.timer('solprops'):
            # Calculate mass errors
            mass = df.assemble(rho_h*dx)
            
            # Calculate the Courant number
            Co_max = solprops.courant_number().vector().max()
            
            # Calculate the divergence
            div_dS_f, div_dx_f = solprops.divergences()
            div_dS = div_dS_f.vector().max()
            div_dx = div_dx_f.vector().max()
            
            # Calculate the energy
            Ek, Ep = total_energy(rho_p, u_h, gvec, x_e)
            
        # Save visualization file
        with log.timer('save'):
            if it % inp.output_step == 0:
                xdmf.save(t)
        
        log.report('TsTime', time.time() - t0_all, '%6.2fs')
        log.report('mass', (mass - mass0), '% 8.4e')
        log.report('min(rho)', rho_h.vector().min(), '%6.3f')
        log.report('max(rho)', rho_h.vector().max(), '%6.3f')
        #log.report('div_dS', div_dS, '%6.3e')
        #log.report('div_dx', div_dx, '%6.3e')
        log.report('div', div_dx+div_dS, '%6.1e')
        log.report('Ek', Ek-Ek0, '%6.2e')
        log.report('Ep', Ep-Ep0, '%6.2e')
        log.report('Co', Co_max, '%6.1e')
        log.report_timestep()
        
        if should_stop():
            log.warning('Stop requested\n')
            break
    
    return t, it


t0_sim = time.time()
try:
    t, it = time_loop()
    ok = True
except:
    # Log the error
    error = traceback.format_exc()
    log.error(error)
    ok = False


mass = df.assemble(rho_h*dx)
log.info('Simulation done in %5.3fs on %d CPUs\n' % (time.time() - t0_sim, NCPUS))
log.info('Ending with mass %.15g\n' % mass)
if ok:
    log.info('Saving to restart file %r\n' % inp.restart_file) 
    save_restart_file(inp.restart_file, inp, log, mesh, restartfuncs, t, it)
log.info('Simulation status: %s\n' % ('SUCCESS' if ok else 'FAILURE'))

if '--plot' in sys.argv:
    from matplotlib import pyplot
    fig = pyplot.figure()
    ax = fig.add_subplot(111, title='Rho')
    df.plot(rho_h, backend='matplotlib')
    pyplot.show()

sys.exit(0 if ok else 1)
