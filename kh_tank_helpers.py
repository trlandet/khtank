# encoding: utf8
from __future__ import division
import os
from math import pi
import numpy
import dolfin as df
from dolfin import dot, dx, ds, dS
import orangutan


NCPUS = df.MPI.size(df.mpi_comm_world())
RANK = df.MPI.rank(df.mpi_comm_world())


class Input(orangutan.Input):
    SECTION = 'KHInput'
    
    # Geometry
    length = 5
    height = 1
    Nx = 500
    Ny = 100
    diagonal = 'crossed'
    
    # Fluid
    rho0 = 1000.0
    drho = 10.0
    Y0 = 0.005/2
    Ad = 0.0
    Ld = 0.5
    g = 9.81
    nu = 0
    rho_initial = 'rho0 - drho/2*tanh(x[1]/Y0)'
    
    # Tank motion
    w0 = pi/6
    w0_dot = 0
    w0_dotdot = 0
    
    # Finite elements
    transport_order = 1
    streamfun_order = 2
    vel_change_order = 0
    vel_proj_method = 'simple'
    transport_space = 'DG'
    streamfun_space = 'CG'
    slope_limiter_rho = 'basic'
    slope_limiter_A = 'basic'
    
    # Time stepping
    dt = 0.01
    tmax = 10.0
    output_step = 25
    
    # Files
    output_file = 'output.log'
    restart_file = 'restart.h5'
    restart_from_file = None
    
    # For testing only
    psi_initial = None


class VelocityProjection():
    def __init__(self, psi, method='simple', change_order=0):
        """
        Project from the stream functipn psi to the velocities u by
        using a local solver on each element
        """
        self.local_solver = None
        self.velocity = None
        assert method == 'simple'
        self._setup_simple_projection(psi, change_order)
        
    def _setup_simple_projection(self, psi, change_order):
        """
        Setup the most simple projection from ψ to u
        
        For each element K in the mesh:
        
            u = - i_3 x ∇ψ
            u ∈ P_{k-1}(K)
        
        Where i_3 is [0, 0, 1] and k is the polynomial degree of ψ,
        
            ψ ∈ P_{k}(Ω)
        
        """
        mesh = psi.function_space().mesh()
        pe = psi.function_space().ufl_element()
        k = pe.degree()
        vel_order = k + change_order
        
        # Function space and trial and test functions
        V = df.VectorFunctionSpace(mesh, 'DG', vel_order)
        u = df.TrialFunction(V)
        v = df.TestFunction(V)
        
        # Define weak form
        w = df.as_vector([psi.dx(1), -psi.dx(0)])
        a = dot(u, v)*dx
        L = dot(w, v)*dx
        
        # Pre-factorize matrices and store for usage in projection
        self.local_solver = df.LocalSolver(a, L)
        self.local_solver.factorize()
        self.velocity = df.Function(V)
    
    def run(self):
        """
        Perform the projection based on the current state of the Function w
        """
        self.local_solver.solve_local_rhs(self.velocity)


class VelocityProjectionPrimal():
    def __init__(self, w, method='bdm'):
        """
        Project from the DG velocity w to the velocities u by
        using a local solver on each element
        """
        self.local_solver = None
        self.velocity = None
        assert method == 'bdm'
        self._setup_bdm_projection(w)
    
    def _setup_bdm_projection(self, w):     
        """
        Implement a modified version of equations 4a and 4b in "Two new
        techniques for generating exactly incompressible approximate
        velocities" by Bernardo Cocburn (2009)

        We want to calulate the velocity field u from the stream function
        ψ such that the resulting velocity field u is agrees on the fluxes
        between two neighbouring elements
        
            u = - i_3 x ∇ψ
            ∇⋅u = 0
            u ∈ H_div
        
        The projection is defined as follows for each element K in the mesh:
        
            <u⋅n, φ> = <û⋅n, φ>  ∀ φ ∈ P_{k}(F) for any face F ∈ ∂K
            (u, ϕ) = (w, ϕ)      ∀ φ ∈ P_{k-2}(K)^2
            (u, ϕ) = (w, ϕ)      ∀ φ ∈ {ϕ ∈ P_{k}(K)^2 : ∇⋅ϕ = 0 in K, ϕ⋅n = 0 on ∂K}  
            
        Here w = k x ∇ψ and û is the flux of w at each face. P_{x} is the
        space of polynomials of order k
        
        The second condition is problematic as it should have been k-1, but
        that would give the first and second condition 13 equations instead
        of the needed 12 
        """
        k = 2
        Vu = w.function_space()
        mesh = Vu.mesh()
        assert w.ufl_shape == (2,)
        assert Vu.ufl_element().degree() == 2
        
        V = Vu#df.VectorFunctionSpace(mesh, 'DG', k)
        n = df.FacetNormal(mesh)
        
        # The mixed function space of the test function
        e1 = df.FiniteElement('DGT', mesh.ufl_cell(), k)
        e2 = df.VectorElement('DG', mesh.ufl_cell(), k-2)
        e3 = df.FiniteElement('Bubble', mesh.ufl_cell(), 3)
        e_mixed  = df.MixedElement([e1, e2, e3])
        W = df.FunctionSpace(mesh, e_mixed)
        v1, v2, v3b = df.TestFunctions(W)
        u = df.TrialFunction(V)
        
        # The fluxes
        u_hat_ds = df.Constant([0, 0])
        incompressibility_flux_type = 'central' # central works slightly better (limited testing)
        if incompressibility_flux_type == 'central':
            u_hat_dS = df.avg(w)
        elif incompressibility_flux_type == 'upwind':
            w_nU = (dot(w, n) + abs(dot(w, n)))/2.0
            switch = df.conditional(df.gt(w_nU('+'), 0.0), 1.0, 0.0)
            u_hat_dS = switch*w('+') + (1 - switch)*w('-')
        
        # Equation 1 - flux through the sides
        a = dot(u, n)*v1*ds
        L = dot(u_hat_ds, n)*v1*ds
        for R in '+-':
            a += dot(u(R), n(R))*v1(R)*dS
            L += dot(u_hat_dS, n(R))*v1(R)*dS
        
        # Equation 2 - internal shape
        a += dot(u, v2)*dx
        L += dot(w, v2)*dx
        
        # Equation 3 - BDM Phi
        v3 = df.as_vector([v3b.dx(1), -v3b.dx(0)]) # Curl of [0, 0, v3b]
        a += dot(u, v3)*dx
        L += dot(w, v3)*dx
        
        # Pre-factorize matrices and store for usage in projection
        self.local_solver = df.LocalSolver(a, L)
        self.local_solver.factorize()
        self.velocity = df.Function(V)
    
    def run(self):
        """
        Perform the projection based on the current state of the Function w
        """
        self.local_solver.solve_local_rhs(self.velocity)


class SlopeLimiter(object):
    def __init__(self, phi, method='basic', use_cpp=True):
        """
        Limit the slope of the given scalar to obtain boundedness
        """
        self.phi = phi
        V = phi.function_space()
        self.degree = V.ufl_element().degree()
        self.mesh = V.mesh()
        
        filter_method = 'nofilter'
        if '+' in method:
            method, filter_method = method.split('+') 
        
        assert V.ufl_element().family() == 'Discontinuous Lagrange'
        assert method == 'basic'
        assert filter_method in ('nofilter', 'minmax')
        assert phi.ufl_shape == ()
        assert self.mesh.topology().dim() == 2
        
        self.method = method
        self.filter = filter_method
        
        # Intermediate function spaces used in the limiter
        self.DG0 = df.FunctionSpace(self.mesh, 'DG', 0)
        self.DG1 = df.FunctionSpace(self.mesh, 'DG', 1)
        self.excedance = df.Function(self.DG0)
        
        # Projection to DG1 with pre factorized matrices for quick application
        if self.degree > 1:
            u, v = df.TrialFunction(self.DG1), df.TestFunction(self.DG1)
            a = dot(u, v)*dx
            L = dot(phi, v)*dx
            self.local_solver1 = df.LocalSolver(a, L)
            self.local_solver1.factorize()
        
        # Fast access to cell dofs
        dm0, dm1 = self.DG0.dofmap(), self.DG1.dofmap()
        indices = range(self.mesh.num_cells())
        self.cell_dofs_DG0 = list(int(dm0.cell_dofs(i)) for i in indices)
        self.cell_dofs_DG1 = list(tuple(dm1.cell_dofs(i)) for i in indices)
        
        # Connectivity from cell to vertex and vice versa
        self.mesh.init(2, 0)
        self.mesh.init(0, 2)
        connectivity_CV = self.mesh.topology()(2, 0)
        connectivity_VC = self.mesh.topology()(0, 2)
        
        # Find the neighbours cells for each vertex
        self.neighbours = []
        for iv in range(self.mesh.num_vertices()):
            cnbs = tuple(connectivity_VC(iv))
            self.neighbours.append(cnbs)
        
        # Find vertices for each cell
        self.vertices = []
        for ic in range(self.mesh.num_cells()):
            vnbs = tuple(connectivity_CV(ic))
            self.vertices.append(vnbs)
        
        self._filter_cache = None
        
        # Get indices for get_local on the DG1 vector
        im = dm1.index_map()
        n_local_and_ghosts = im.size(im.MapSize_ALL)
        intc = numpy.intc
        self.local_indices_dg1 = numpy.arange(n_local_and_ghosts, dtype=intc)
        
        self.use_cpp = use_cpp
        if use_cpp:
            self.num_neighbours = numpy.array([len(nbs) for nbs in self.neighbours], dtype=intc)
            self.max_neighbours = Nnmax = self.num_neighbours.max()
            self.flat_neighbours = numpy.zeros(len(self.neighbours)*Nnmax, dtype=intc) - 1
            for i, nbs in enumerate(self.neighbours):
                self.flat_neighbours[i*Nnmax:i*Nnmax + self.num_neighbours[i]] = self.neighbours[i]
            self.flat_cell_dofs = numpy.array(self.cell_dofs_DG1, dtype=intc).flatten()
            self.flat_cell_dofs_dg0 = numpy.array(self.cell_dofs_DG0, dtype=intc).flatten()
            self.flat_vertices = numpy.array(self.vertices, dtype=intc).flatten()
            
            cpp_dir = os.path.dirname(os.path.abspath(__file__))
            self.cpp_mod = orangutan.load_cpp_module(['slope_limiter_basic.h'], [], cpp_dir)
    
    def run(self):
        """
        Perform the slope limiting and filtering
        """
        assert self.degree == 1, 'Slope limiter only implemented for linear elements'
        
        # Get local values + the ghost values
        results = numpy.zeros(len(self.local_indices_dg1), float)
        self.phi.vector().get_local(results, self.local_indices_dg1)
        
        if self.degree > 0:
            if self.use_cpp:
                self._run_basic_limiter_cpp(results)
            else:
                self._run_basic_limiter(results)
        
        if self.filter == 'minmax':
            self._run_minmax_filter(results)
        
        self.phi.vector().set_local(results, self.local_indices_dg1)
        self.phi.vector().apply('insert')
    
    def _run_basic_limiter_cpp(self, results):
        num_cells_all = self.mesh.num_cells()
        tdim = self.mesh.topology().dim()
        num_cells_owned = self.mesh.topology().ghost_offset(tdim)
        exceedances = self.excedance.vector().get_local()
        self.cpp_mod.slope_limiter_basic_cg1(self.num_neighbours,
                                             num_cells_all,
                                             num_cells_owned,
                                             self.max_neighbours,
                                             self.flat_neighbours,
                                             self.flat_cell_dofs,
                                             self.flat_cell_dofs_dg0,
                                             self.flat_vertices,
                                             exceedances,
                                             results)
        self.excedance.vector().set_local(exceedances)
        self.excedance.vector().apply('insert')
    
    def _run_basic_limiter(self, results):
        """
        Perform basic slope limiting
        """
        exceedances = self.excedance.vector().get_local()
        
        # Get number of non-ghosts
        assert self.mesh.topology().dim() ==  2
        ncells = self.mesh.topology().ghost_offset(2)
        
        # Cell averages
        averages = []
        for ic in range(self.mesh.num_cells()):
            dofs = self.cell_dofs_DG1[ic]
            vals = [results[dof] for dof in dofs]
            averages.append(sum(vals)/3)
        
        # Modify vertex values
        onetwothree = range(3)
        for ic in range(ncells):
            vertices = self.vertices[ic]
            dofs = self.cell_dofs_DG1[ic]
            vals = [results[dof] for dof in dofs]
            avg = sum(vals)/3
            
            excedance = 0
            for ivertex in onetwothree:
                vtx = vertices[ivertex]
                nbs = self.neighbours[vtx]
                nb_vals = [averages[self.cell_dofs_DG0[nb]] for nb in nbs]
                
                # Find highest and lowest value in the connected cells
                lo, hi = 1e100, -1e100
                for cell_avg in nb_vals:
                    lo = min(lo, cell_avg)
                    hi = max(hi, cell_avg)
                
                vtx_val = vals[ivertex] 
                if vtx_val < lo:
                    vals[ivertex] = lo
                    ex = vtx_val - lo
                    if abs(excedance) < abs(ex):
                        excedance = ex
                elif vtx_val > hi:
                    vals[ivertex] = hi
                    ex = vtx_val - hi
                    if abs(excedance) < abs(ex):
                        excedance = ex
            
            exceedances[self.cell_dofs_DG0[ic]] = excedance
            if excedance == 0:
                continue
            
            # Modify the results to limit the slope
            
            # Find the new average and which vertices can be adjusted to obtain the correct average
            new_avg = sum(vals)/3
            eps = 0
            moddable = [0, 0, 0]
            if abs(avg - new_avg) > 1e-15:
                if new_avg > avg:
                    for ivertex in onetwothree:
                        if vals[ivertex] > avg:
                            moddable[ivertex] = 1
                else:
                    for ivertex in onetwothree:
                        if vals[ivertex] < avg:
                            moddable[ivertex] = 1
                
                # Get number of vertex values that can be modified and catch
                # possible floating point problems with the above comparisons
                nmod = sum(moddable)
                
                if nmod == 0:
                    assert abs(excedance) < 1e-14, 'Nmod=0 with exceedance=%r' % excedance
                else:
                    eps = (avg - new_avg)*3/nmod
            
            # Modify the vertices to obtain the correct average
            for iv in onetwothree:
                results[dofs[iv]] = vals[iv] + eps*moddable[iv]
        
        self.excedance.vector().set_local(exceedances)
        self.excedance.vector().apply('insert')
    
    def _run_minmax_filter(self, results):
        """
        Make sure the DOF values are inside the min and max values from the
        first time step (enforce 
        """
        if self._filter_cache is None:
            mi = df.MPI.min(df.mpi_comm_world(), float(results.min()))
            ma = df.MPI.max(df.mpi_comm_world(), float(results.max()))
            self._filter_cache = mi, ma
            return
        minval, maxval = self._filter_cache
        numpy.clip(results, minval, maxval, results)


class SolutionProperties(object):
    def __init__(self, mesh, u, dt, nu, divergence='div'):
        """
        Calculate Courant and Peclet numbers
        """
        self.mesh = mesh
        
        if dt != 0:
            self._setup_courant(u, dt)
        if nu != 0:
            self._setup_peclet(u, nu)
        if divergence:
            self._setup_divergence(u, divergence)
    
    def _setup_courant(self, vel, dt):
        """
        Co = a*dt/h where a = mag(vel)
        """
        V = df.FunctionSpace(self.mesh, 'DG', 0)
        h = df.CellSize(self.mesh)
        u, v = df.TrialFunction(V), df.TestFunction(V)
        a = u*v*dx
        vmag = df.sqrt(dot(vel, vel))
        L = vmag*dt/h*v*dx
        
        # Pre-factorize matrices and store for usage in projection
        self._courant_solver = df.LocalSolver(a, L)
        self._courant_solver.factorize()
        self._courant = df.Function(V)
    
    def _setup_peclet(self, vel, nu):
        """
        Pe = a*h/(2*nu) where a = mag(vel)
        """
        V = df.FunctionSpace(self.mesh, 'DG', 0)
        h = df.CellSize(self.mesh)
        df_nu = df.Constant(nu)
        u, v = df.TrialFunction(V), df.TestFunction(V)
        a = u*v*dx
        L = dot(vel, vel)**0.5*h/(2*df_nu)*v*dx
        
        # Pre-factorize matrices and store for usage in projection
        self._peclet_solver = df.LocalSolver(a, L)
        self._peclet_solver.factorize()
        self._peclet = df.Function(V)
    
    def _setup_divergence(self, vel, method):
        """
        Calculate divergence and element to element velocity
        flux differences on the same edges
        """
        V = df.FunctionSpace(self.mesh, 'DG', 0)
        n = df.FacetNormal(self.mesh)
        u, v = df.TrialFunction(V), df.TestFunction(V)
        
        # The difference between the flux on the same facet between two different cells
        a1 = u*v*dx
        w = dot(vel('+') - vel('-'), n('+'))
        L1 = abs(w)*df.avg(v)*dS
        
        # The divergence internally in the cell
        a2 = u*v*dx
        if method == 'div':
            L2 = abs(df.div(vel))*v*dx
        elif method == 'gradq_avg':
            L2 = dot(df.avg(vel), n('+'))*df.jump(v)*dS - dot(vel, df.grad(v))*dx
        else:
            raise ValueError('Divergence type %r not supported' % method)
        
        # Pre-factorize matrices and store for usage in projection
        self._div_dS_solver = df.LocalSolver(a1, L1)
        self._div_dx_solver = df.LocalSolver(a2, L2)
        self._div_dS_solver.factorize()
        self._div_dx_solver.factorize()
        self._div_dS = df.Function(V)
        self._div_dx = df.Function(V)
    
    def courant_number(self):
        """
        Calculate the Courant numbers in each cell
        """
        self._courant_solver.solve_local_rhs(self._courant)
        return self._courant
    
    def peclet_number(self):
        """
        Calculate the Peclet numbers in each cell
        """
        self._peclet_solver.solve_local_rhs(self._peclet)
        return self._peclet
    
    def divergences(self):
        """
        Calculate the difference between the flux on the same
        facet between two different cells and the divergence
        inside each cell.
        
        Returns the sum of facet errors for each cell and the
        divergence error in each cell as DG0 functions 
        """
        self._div_dS_solver.solve_global_rhs(self._div_dS)
        self._div_dx_solver.solve_global_rhs(self._div_dx)
        return self._div_dS, self._div_dx


class HydrostaticPressure(object):
    def __init__(self, rho, g, Vp, center=(0, 0), eps=1e-8):
        """
        Calculate the hydrostatic pressure
        """
        self.mesh = Vp.mesh()
        self.p_hydrostatic = df.Function(Vp)
        self.pi = df.Function(Vp)
        
        # Create null space vector in Vp Space
        null_func = df.Function(Vp)
        null_vec = null_func.vector()
        null_vec[:] = 1
        null_vec *= 1/null_vec.norm("l2")
        self.null_space = df.VectorSpaceBasis([null_func.vector()])
        
        # Define the forms
        p = df.TrialFunction(Vp)
        q = df.TestFunction(Vp)
        
        self.A = []
        self.L = []
        self.bcs = []
        self.solvers = []
        for d in range(2):
            a = p.dx(d)*q.dx(d)*dx
            A = df.assemble(a)
            L = g[d]*rho*q.dx(d)*dx
            
            #df.as_backend_type(A).set_nullspace(self.null_space)
            self.A.append(A)
            self.L.append(L)
            
            inside = lambda  x, on_boundary: center[d] - eps <= x[d] <= center[d] + eps
            bc = df.DirichletBC(Vp, 0.0, inside)
            self.bcs.append(bc)
            
            solver = df.PETScKrylovSolver('gmres', 'hypre_amg')
            solver.parameters['relative_tolerance'] = 1e-15
            solver.parameters['absolute_tolerance'] = 1e-15
            solver.parameters['preconditioner']['structure'] = 'same'
            self.solvers.append(solver)
    
    def update(self):
        self.p_hydrostatic.vector().zero()
        for A, L, bc, solver in zip(self.A, self.L, self.bcs, self.solvers):
            b = df.assemble(L)
            #self.null_space.orthogonalize(b)
            bc.apply(A, b)
            solver.solve(A, self.pi.vector(), b)
            self.p_hydrostatic.vector().axpy(1, self.pi.vector())
        
        # Removing the null space of the matrix system is not strictly the same as removing
        # the null space of the equation, so we correct for this here
        vol = df.assemble(df.Constant(1)*dx(domain=self.mesh))
        p_avg = df.assemble(self.p_hydrostatic*dx(domain=self.mesh))/vol
        self.p_hydrostatic.vector()[:] -= p_avg


def total_energy(rho, u, gvec, x):
    """
    Calculate the total energy in the field
    """
    form_E_k = 1/2*rho*dot(u, u)*dx
    form_E_p = rho*dot(-gvec, x)*dx
    E_k = df.assemble(form_E_k)
    E_p = df.assemble(form_E_p)
    return E_k, E_p


def define_penalty(mesh, P, k_min, k_max, boost_factor=3, exponent=1, log=None):
    """
    Define the penalty parameter used in the Poisson equations
    
    Arguments:
        mesh: the mesh used in the simulation
        P: the polynomial degree of the unknown
        k_min: the minimum diffusion coefficient
        k_max: the maximum diffusion coefficient
        boost_factor: the penalty is multiplied by this factor
        exponent: set this to greater than 1 for superpenalisation
    """
    assert k_max >= k_min
    ndim = mesh.geometry().dim()
    
    # Calculate geometrical factor used in the penalty
    geom_fac = 0
    for cell in df.cells(mesh):
        vol = cell.volume()
        area = sum(cell.facet_area(i) for i in range(ndim + 1))
        gf = area/vol
        geom_fac = max(geom_fac, gf)
    geom_fac = df.MPI.max(df.mpi_comm_world(), float(geom_fac))
    
    penalty = boost_factor * k_max**2/k_min * (P + 1)*(P + ndim)/ndim * geom_fac**exponent
    
    if log:
        log.info('Penalty %.5g\n' % penalty)
    
    return df.Constant(penalty), df.Constant(penalty*2) 
