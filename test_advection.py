# encoding: utf8
from __future__ import division
import time, traceback, sys
import dolfin as df
from dolfin import grad, dot, dx, ds, dS, cos, sin, jump, avg
from kh_tank_helpers import VelocityProjection, SlopeLimiter, SolutionProperties, define_penalty
from kh_tank_helpers import XDMFSaver, SimpleLog, NCPUS, should_stop, Input


###############################################################################
# Input data

inpfile = sys.argv[1]
print 'Reading input file %s' % inpfile
inp = Input()
inp.read(inpfile)

use_supg_stabilization = (inp.transport_space == 'CG')


###############################################################################
# Preliminaries

# Setup logging
log = SimpleLog(inp.output_file)
df.set_log_level(df.WARNING)
log.info('Running advection test with input:\n')
log.info(str(inp))
log.info('---------\n')

# Mesh and function spaces
p0 = df.Point(-inp.length/2, -inp.height/2)
p1 = df.Point(inp.length/2, inp.height/2)
mesh = df.RectangleMesh(p0, p1, inp.Nx, inp.Ny, inp.diagonal)
V1 = df.FunctionSpace(mesh, inp.transport_space, inp.transport_order)
V3 = df.FunctionSpace(mesh, inp.streamfun_space, inp.streamfun_order)

# Initial condition for the density
el = V1.ufl_element()
params = dict(rho0=inp.rho0, drho=inp.drho, Y0=inp.Y0, Ad=inp.Ad, Ld=inp.Ld)
rho_expr = df.Expression(inp.rho_initial, element=el, **params)
mass0 = df.assemble(rho_expr*dx(mesh))
log.info('Starting with mass %.15g\n' % mass0)

# Initial condition for the velocity
el = V3.ufl_element()
psi_expr = df.Expression(inp.psi_initial, element=el, **params)

# Current time step
rho_h = df.interpolate(rho_expr, V1)
psi_h = df.interpolate(psi_expr, V3)

# Velocity calculated from psi locally on each element
t1 = time.time()
velproj = VelocityProjection(psi_h, method=inp.vel_proj_method)
u_h = velproj.velocity
log.info('Set up VelocityProjection %s in %.2fs\n' % (inp.vel_proj_method, time.time() - t1))

# Give names for output file
rho_h.rename('rho', 'rho')
psi_h.rename('psi', 'psi')
u_h.rename('u', 'u')

# Slope limiters
if inp.slope_limiter_rho != 'none':
    slope_limiter_rho = SlopeLimiter(rho_h, inp.slope_limiter_rho)
    sle_rho = slope_limiter_rho.excedance
    sle_rho.rename('sleRho', 'Slope limiter exceedance for rho')

# Dolfin constants and functions
df_dt = df.Constant(inp.dt)
n = df.FacetNormal(mesh)
h = df.CellSize(mesh)
zero = df.Constant(0.0)


###############################################################################
# Weak forms

rho = df.TrialFunction(V1)
v1 = df.TestFunction(V1)

# SUPG stabilization
if use_supg_stabilization:
    # Velocity magnitude
    a = df.sqrt(dot(u_h, u_h))
    # Stabilization parameter (Donea & Huerta - page 65)
    tau = ((2*a/h)**2 + 1/df_dt**2)**-0.5
    
    # Avoid exessive quadrature order ...
    # This should be replaced with a UFL hint of some sort ...
    Vtau = df.FunctionSpace(mesh, 'DG', 0)
    tau2 = df.Function(Vtau)
    utau, vtau = df.TrialFunction(Vtau), df.TestFunction(Vtau)
    tau_solver = df.LocalSolver(utau*vtau*dx, tau*vtau*dx)
    tau_solver.factorize()
    
    v1_supg = dot(u_h, grad(v1))*tau2

# Upwind velocity
upw = (abs(dot(u_h, n)) + dot(u_h, n))/2

# Equation 1 - transport equation for rho
eq1 = (rho - rho_h)/df_dt*v1*dx
eq1 -= rho*dot(u_h, grad(v1))*dx
if inp.transport_space == 'DG':
    F1 = jump(rho*upw)
    eq1 += F1*jump(v1)*dS
if use_supg_stabilization:
    R1 = (rho - rho_h)/df_dt + dot(u_h, grad(rho))
    eq1 += R1*v1_supg*dx
a1, L1 = df.system(eq1)


###############################################################################
# Linear equation solvers

solver1 = df.KrylovSolver('gmres', 'default')

for solver in (solver1,):
    solver.parameters['nonzero_initial_guess'] = True
    solver.parameters['relative_tolerance'] = 1e-10
    solver.parameters['absolute_tolerance'] = 1e-10
    solver.parameters['preconditioner']['structure'] = 'same_nonzero_pattern'


###############################################################################
# Run simulation

# Prepare XDMF output file to store results for visualisation
xdmf_funcs = [rho_h, psi_h, u_h]
if inp.slope_limiter_rho != 'none':
    xdmf_funcs.append(slope_limiter_rho.excedance)
xdmf = XDMFSaver(xdmf_funcs, 'results')

# Calculate solution properties
solprops = SolutionProperties(u_h, df_dt, nu=0, divergence='div')

# These do not change since Psi is constant in time
velproj.run()
if use_supg_stabilization:
    tau_solver.solve_local_rhs(tau2)
A1 = df.assemble(a1)


def time_loop():
    t = 0.0
    it = 0
    xdmf.save(t)
    while t <= inp.tmax - inp.dt + 1e-6:
        it += 1
        t += inp.dt
        t0_all = time.time()
        log.report('Time', t, '%7.3f')
        
        with log.timer('assemble'):
            b1 = df.assemble(L1)
        
        with log.timer('solve'):
            solver1.solve(A1, rho_h.vector(), b1)
        
        with log.timer('limit'):
            if inp.slope_limiter_rho != 'none':
                slope_limiter_rho.run()
        
        with log.timer('analyse'):
            # Calculate mass errors
            mass = df.assemble(rho_h*dx)
            
            # Calculate the Courant number
            Co_max = solprops.courant_number().vector().max()
            
            # Calculate the divergence
            div_dS_f, div_dx_f = solprops.divergences()
            div_dS = div_dS_f.vector().max()
            div_dx = div_dx_f.vector().max()
            
        # Save visualization file
        with log.timer('save'):
            if it % inp.output_step == 0:
                xdmf.save(t)
        
        log.report('TsTime', time.time() - t0_all, '%6.2fs')
        log.report('mass', (mass - mass0), '% 8.4e')
        log.report('min(rho)', rho_h.vector().min(), '%6.3f')
        log.report('max(rho)', rho_h.vector().max(), '%6.3f')
        log.report('div_dS', div_dS, '%6.3e')
        log.report('div_dx', div_dx, '%6.3e')
        log.report('Co', Co_max, '%6.1e')
        log.report_timestep()
        
        if should_stop():
            log.warning('Stop requested\n')
            break

t0_sim = time.time()
try:
    time_loop()
    ok = True
except:
    # Log the error
    error = traceback.format_exc()
    log.error(error)
    ok = False

mass = df.assemble(rho_h*dx)
log.info('Simulation done in %5.3fs on %d CPUs\n' % (time.time() - t0_sim, NCPUS))
log.info('Ending with mass %.15g\n' % mass)
log.info('Simulation status: %s\n' % ('SUCCESS' if ok else 'FAILURE'))
sys.exit(0 if ok else 1)

