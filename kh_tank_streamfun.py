# encoding: utf8
from __future__ import division
import time, traceback, sys
import dolfin as df
from dolfin import grad, dot, dx, ds, dS, cos, sin, jump, avg
from kh_tank_helpers import VelocityProjection, SlopeLimiter, SolutionProperties, define_penalty, Input, total_energy
from orangutan import XDMFSaver, Log, should_stop, save_restart_file, load_restart_file, NCPUS, PyCode


###############################################################################
# Input data

inpfile = sys.argv[1]
print 'Reading input file %s' % inpfile
inp = Input()
inp.read(inpfile)

use_supg_stabilization = (inp.transport_space == 'CG')


###############################################################################
# Preliminaries

# Setup logging
log = Log(inp.output_file)
df.set_log_level(df.WARNING)
log.info('Running KH Tank with input:\n')
log.info(str(inp))
log.info('---------\n')

# Mesh and function spaces
df.parameters['ghost_mode'] = 'shared_vertex'
p0 = df.Point(-inp.length/2, -inp.height/2)
p1 = df.Point(inp.length/2, inp.height/2)
mesh = df.RectangleMesh(p0, p1, inp.Nx, inp.Ny, inp.diagonal)
V1 = df.FunctionSpace(mesh, inp.transport_space, inp.transport_order)
V3 = df.FunctionSpace(mesh, inp.streamfun_space, inp.streamfun_order)
DG0 =  df.FunctionSpace(mesh, 'DG', 0)

# Current time step
rho_h = df.Function(V1)
A_h = df.Function(V1)
psi_h = df.Function(V3)

# Velocity calculated from psi locally on each element
t1 = time.time()
velproj = VelocityProjection(psi_h, method=inp.vel_proj_method,
                             change_order=inp.vel_change_order)
u_h = velproj.velocity
log.info('Set up VelocityProjection %s in %.2fs\n' % (inp.vel_proj_method, time.time() - t1))

# Give names for output file
rho_h.rename('rho', 'rho')
A_h.rename('A', 'A')
psi_h.rename('psi', 'psi')
u_h.rename('u', 'u')

# Previous time steps
rho_p = df.Function(V1, )
rho_pp = df.Function(V1)
A_p = df.Function(V1)
A_pp = df.Function(V1)
for func, name in [(rho_p, 'rho_p'), (rho_pp, 'rho_pp'), (A_p, 'A_p'), (A_pp, 'A_pp')]:
    func.rename(name, name)
    
# Initial condition
el = V1.ufl_element()
params = dict(rho0=inp.rho0, drho=inp.drho, Y0=inp.Y0, Ad=inp.Ad, Ld=inp.Ld)
rho_expr = df.Expression(inp.rho_initial, element=el, **params)
rho_p.assign(df.interpolate(rho_expr, V1))
rho_h.assign(rho_p)

# Slope limiters
if inp.slope_limiter_rho != 'none':
    slope_limiter_rho = SlopeLimiter(rho_h, inp.slope_limiter_rho)
    sle_rho = slope_limiter_rho.excedance
    sle_rho.rename('sleRho', 'Slope limiter exceedance for rho')

if inp.slope_limiter_A != 'none':
    slope_limiter_A = SlopeLimiter(A_h, inp.slope_limiter_A)
    sle_A = slope_limiter_A.excedance
    sle_A.rename('sleA', 'Slope limiter exceedance for A')

# Dolfin constants and functions
df_g = df.Constant(inp.g)
df_dt = df.Constant(inp.dt)
df_rho0 = df.Constant(inp.rho0)
df_nu = df.Constant(inp.nu)*rho_p
n = df.FacetNormal(mesh)
h = df.CellSize(mesh)
x = df.SpatialCoordinate(mesh)
zero = df.Constant(0.0)
tc = df.Constant([1.0, -1.0, 0.0])

# Tank motion
code_w0 = PyCode(inp.w0, 'input w0')
code_w0_dot = PyCode(inp.w0_dot, 'input w0_dot')
code_w0_dotdot = PyCode(inp.w0_dotdot, 'input w0_dotdot')
df_w0 = df.Constant(0.0)
df_w0_dot = df.Constant(0.0)
df_w0_dotdot = df.Constant(0.0)
gvec = df.as_vector([df_g*sin(df_w0), -df_g*cos(df_w0)])
def move_tank(t, it):
    df_w0.assign(code_w0.run(t=t, it=it, inp=inp))
    df_w0_dot.assign(code_w0_dot.run(t=t, it=it, inp=inp))
    df_w0_dotdot.assign(code_w0_dotdot.run(t=t, it=it, inp=inp))
move_tank(0.0, 0)

# Starting values
mass0 = df.assemble(rho_p*dx(mesh))
x_e = df.as_vector([x[0], x[1]+df.Constant(inp.height/2)])
Ek0, Ep0 = total_energy(rho_p, u_h, gvec, x_e)
log.info('Starting with mass %.15g\n' % mass0)
log.info('Starting with kinetic energy   %15.5e\n' % Ek0)
log.info('Starting with potential energy %15.5e\n' % Ep0)


###############################################################################
# Weak forms

rho = df.TrialFunction(V1)
A = df.TrialFunction(V1)
psi = df.TrialFunction(V3)
v1 = df.TestFunction(V1)
v3 = df.TestFunction(V3)

# SUPG stabilization
if use_supg_stabilization:
    # Velocity magnitude
    a = df.sqrt(dot(u_h, u_h))
    # Stabilization parameter (Donea & Huerta - page 65)
    tau = ((2*a/h)**2 + 1/df_dt**2)**-0.5
    
    # Avoid exessive quadrature order ...
    # This should be replaced with a UFL hint of some sort ...
    Vtau = df.FunctionSpace(mesh, 'DG', 0)
    tau2 = df.Function(Vtau)
    utau, vtau = df.TrialFunction(Vtau), df.TestFunction(Vtau)
    tau_solver = df.LocalSolver(utau*vtau*dx, tau*vtau*dx)
    tau_solver.factorize()
    
    v1_supg = dot(u_h, grad(v1))*tau2

# Upwind velocity
upw = (abs(dot(u_h, n)) + dot(u_h, n))/2

# Equation 1 - transport equation for rho
eq1 = (tc[0]*rho + tc[1]*rho_p + tc[2]*rho_pp)/df_dt*v1*dx
eq1 -= rho*dot(u_h, grad(v1))*dx
if inp.transport_space == 'DG':
    F1 = jump(rho*upw)
    eq1 += F1*jump(v1)*dS
if use_supg_stabilization:
    R1 = (tc[0]*rho + tc[1]*rho_p + tc[2]*rho_pp)/df_dt + dot(u_h, grad(rho))
    eq1 += R1*v1_supg*dx
a1, L1 = df.system(eq1)

# Equation 2 - transport equation for A
eq2 = (tc[0]*A + tc[1]*A_p + tc[2]*A_pp)/df_dt*v1*dx
eq2 -= A*dot(u_h, grad(v1))*dx
rhs2 = rho_h.dx(0)*dot(u_h, u_h).dx(1)/2
rhs2 -= rho_h.dx(1)*dot(u_h, u_h).dx(0)/2
rhs2 += df_g*(-cos(df_w0)*rho_h.dx(0) + sin(df_w0)*rho_h.dx(1))
rhs2 -= df_w0_dotdot*(x[0]*rho_h.dx(0) + x[1]*rho_h.dx(1) - 2*rho_h)
rhs2 += df_w0_dot**2*(x[1]*rho_h.dx(0) - x[0]*rho_h.dx(1))
rhs2 -= 2*df_w0_dot*(u_h[0]*rho_h.dx(0) + u_h[1]*rho_h.dx(1))
eq2 -= rhs2*v1*dx(domain=mesh)
if inp.transport_space == 'DG':
    F2 = jump(A*upw)  
    eq2 += F2*jump(v1)*dS   
if inp.nu > 0 and inp.transport_space == 'DG':
    # Viscosity
    eq2 += df_nu*dot(grad(A), grad(v1))*dx
    
    # Penalties
    P = V1.ufl_element().degree()
    k_min, k_max = inp.nu*rho_p.vector().min(), inp.nu*rho_p.vector().max()
    penalty_dS, penalty_ds = define_penalty(mesh, P, k_min, k_max, log=log)
    
    # Symmetric Interior Penalty method for -∇⋅∇ψ
    eq2 -= dot(n('+'), avg(df_nu*grad(A)))*jump(v1)*dS
    eq2 -= dot(n('+'), avg(df_nu*grad(v1)))*jump(A)*dS
    
    # Weak continuity / coersivity term
    eq2 += penalty_dS*jump(A)*jump(v1)*dS
    
    # No BC
    eq2 -= dot(n, df_nu*grad(A))*v1*ds
    #eq2 -= dot(n, df_nu*grad(v1))*A*ds
    #eq2 += dot(n, df_nu*grad(v1))*zero*ds
    #eq2 += penalty_ds*psi*v1*ds
    #eq2 -= penalty_ds*zero*v1*ds
if use_supg_stabilization:
    R2 = (tc[0]*A + tc[1]*A_p + tc[2]*A_pp)/df_dt + dot(u_h, grad(A)) - rhs2
    eq2 += R2*v1_supg*dx
a2, L2 = df.system(eq2)

# Equation 3 - Poisson equation for psi
eq3 = rho_h*dot(grad(psi), grad(v3))*dx
eq3 -= A_h*v3*dx
if inp.streamfun_space == 'DG':
    # Penalties
    P = V3.ufl_element().degree()
    k_min, k_max = rho_h.vector().min(), rho_h.vector().max()
    penalty_dS, penalty_ds = define_penalty(mesh, P, k_min, k_max, log=log)
    
    # Symmetric Interior Penalty method for -∇⋅∇ψ
    eq3 -= dot(n('+'), avg(rho_h*grad(psi)))*jump(v3)*dS
    eq3 -= dot(n('+'), avg(rho_h*grad(v3)))*jump(psi)*dS
    
    # Weak continuity / coersivity term
    eq3 += penalty_dS*jump(psi)*jump(v3)*dS
    
    # Weak Dirichlet:
    eq3 -= dot(n, rho_h*grad(psi))*v3*ds
    eq3 -= dot(n, rho_h*grad(v3))*psi*ds
    eq3 += dot(n, rho_h*grad(v3))*zero*ds
    eq3 += penalty_ds*psi*v3*ds
    eq3 -= penalty_ds*zero*v3*ds
a3, L3 = df.system(eq3)


###############################################################################
# Boundary conditions

bcs3 = None
if inp.streamfun_space != 'DG':
    bc_psi = df.DirichletBC(V3, zero, lambda x, on_boundary: on_boundary)
    bcs3 = [bc_psi]


###############################################################################
# Linear equation solvers

solver1 = df.KrylovSolver('gmres', 'default')
solver2 = df.KrylovSolver('gmres', 'default')
solver3 = df.KrylovSolver('gmres', 'hypre_amg')

for solver in (solver1, solver2, solver3):
    solver.parameters['nonzero_initial_guess'] = True
    solver.parameters['relative_tolerance'] = 1e-15
    solver.parameters['absolute_tolerance'] = 1e-15
    solver.parameters['preconditioner']['structure'] = 'same_nonzero_pattern'


###############################################################################
# Run simulation

# Prepare XDMF output file to store results for visualisation
xdmf_funcs = [rho_h, A_h, psi_h, u_h]
if inp.slope_limiter_rho != 'none':
    xdmf_funcs.append(slope_limiter_rho.excedance)
if inp.slope_limiter_A != 'none':
    xdmf_funcs.append(slope_limiter_A.excedance)
xdmf = XDMFSaver(xdmf_funcs, 'results')

# Calculate solution properties
solprops = SolutionProperties(mesh, u_h, df_dt, inp.nu, divergence='gradq_avg')

# Functions needed to restart
restartfuncs = [rho_h, rho_pp, A_h, A_pp, psi_h, u_h]

def time_loop():
    t = 0.0
    it = 0
    xdmf.save(t)
    while t <= inp.tmax - inp.dt + 1e-6:
        it += 1
        t += inp.dt
        t0_all = time.time()
        
        # Restart from file
        if it == 1 and inp.restart_from_file:
            log.info('Loading restart file %r\n' % inp.restart_from_file)
            t, it = load_restart_file(inp.restart_from_file, restartfuncs, log)
            rho_p.assign(rho_h)
            A_p.assign(A_h)
        
        # Update to second order timestepping
        if it > 1:
            tc.assign(df.Constant([1.5, -2.0, 0.5]))
        
        log.report('Time', t, '%7.3f')
        move_tank(t, it)
        
        # Update SUPG stabilization parameter tau
        if use_supg_stabilization:
            tau_solver.solve_local_rhs(tau2)
        
        # Linear solve 1 - find rho
        with log.timer('rho'):
            A1, b1 = df.assemble_system(a1, L1)
            solver1.solve(A1, rho_h.vector(), b1)
            if inp.slope_limiter_rho != 'none':
                slope_limiter_rho.run()
        
        # Linear solve 2 - find A
        with log.timer('A'):
            A2, b2 = df.assemble_system(a2, L2)
            solver2.solve(A2, A_h.vector(), b2)
            if inp.slope_limiter_A != 'none':
                slope_limiter_A.run()
        
        # Linear solve 3 - find psi
        with log.timer('psi'):
            A3, b3 = df.assemble_system(a3, L3, bcs3)
            solver3.solve(A3, psi_h.vector(), b3)
            
            # Project -k x grad(psi)) into u
            # (this is very fast, no need for separate timer)
            velproj.run()
            
        # Advance in time
        rho_pp.assign(rho_p)
        rho_p.assign(rho_h)
        A_pp.assign(A_p)
        A_p.assign(A_h)
        
        with log.timer('solprops'):
            # Calculate mass errors
            mass = df.assemble(rho_h*dx)
            
            # Calculate the Courant number
            Co_max = solprops.courant_number().vector().max()
            
            # Calculate the divergence
            div_dS_f, div_dx_f = solprops.divergences()
            div_dS = div_dS_f.vector().max()
            div_dx = div_dx_f.vector().max()
            
            # Calculate the energy
            Ek, Ep = total_energy(rho_p, u_h, gvec, x_e)
            
        # Save visualization file
        with log.timer('save'):
            if it % inp.output_step == 0:
                xdmf.save(t)
        
        log.report('TsTime', time.time() - t0_all, '%6.2fs')
        log.report('mass', (mass - mass0), '% 8.4e')
        log.report('min(rho)', rho_h.vector().min(), '%6.3f')
        log.report('max(rho)', rho_h.vector().max(), '%6.3f')
        #log.report('div_dS', div_dS, '%6.3e')
        #log.report('div_dx', div_dx, '%6.3e')
        log.report('div', div_dx+div_dS, '%6.1e')
        log.report('Ek', Ek-Ek0, '%6.2e')
        log.report('Ep', Ep-Ep0, '%6.2e')
        log.report('Co', Co_max, '%6.1e')
        log.report_timestep()
        
        if should_stop():
            log.warning('Stop requested\n')
            break
    
    return t, it


t0_sim = time.time()
try:
    t, it = time_loop()
    ok = True
except:
    # Log the error
    error = traceback.format_exc()
    log.error(error)
    ok = False


mass = df.assemble(rho_h*dx)
log.info('Simulation done in %5.3fs on %d CPUs\n' % (time.time() - t0_sim, NCPUS))
log.info('Ending with mass %.15g\n' % mass)
if ok:
    log.info('Saving to restart file %r\n' % inp.restart_file) 
    save_restart_file(inp.restart_file, inp, log, mesh, restartfuncs, t, it)
log.info('Simulation status: %s\n' % ('SUCCESS' if ok else 'FAILURE'))

if '--plot' in sys.argv:
    from matplotlib import pyplot
    fig = pyplot.figure()
    ax = fig.add_subplot(111, title='Rho')
    df.plot(rho_h, backend='matplotlib')
    pyplot.show()

sys.exit(0 if ok else 1)
