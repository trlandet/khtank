# encoding: utf8
from __future__ import division
import time, traceback, sys
import dolfin as df
from dolfin import grad, div, dot, dx, ds, dS, cos, sin, jump, avg
from kh_tank_helpers import SlopeLimiter, VelocityProjectionPrimal, SolutionProperties, Input, total_energy, define_penalty
from orangutan import XDMFSaver, Log, should_stop, save_restart_file, load_restart_file, NCPUS, PyCode


###############################################################################
# Input data

inpfile = sys.argv[1]
print 'Reading input file %s' % inpfile
inp = Input()
Input.space_rho = 'DG'
Input.order_rho = 1
Input.space_u = 'DG'
Input.order_u = 2
Input.space_p = 'DG'
Input.order_p = 1
Input.p_initial = '0'
inp.read(inpfile)


###############################################################################
# Preliminaries

# Setup logging
log = Log(inp.output_file)
df.set_log_level(df.WARNING)
log.info('Running KH Tank with input:\n')
log.info(str(inp))
log.info('---------\n')

# Mesh and function spaces
df.parameters['ghost_mode'] = 'shared_vertex'
p0 = df.Point(-inp.length/2, -inp.height/2)
p1 = df.Point(inp.length/2, inp.height/2)
mesh = df.RectangleMesh(p0, p1, inp.Nx, inp.Ny, inp.diagonal)
Vr = df.FunctionSpace(mesh, inp.space_rho, inp.order_rho)
Vu = df.VectorFunctionSpace(mesh, inp.space_u, inp.order_u)
Vp = df.FunctionSpace(mesh, inp.space_p, inp.order_p)

# Mixed function space, assigners, and mixed function
func_spaces = [Vr, Vu.sub(0), Vu.sub(1), Vp]
e_mixed = df.MixedElement([fs.ufl_element() for fs in func_spaces])
W = df.FunctionSpace(mesh, e_mixed)
fa_split = df.FunctionAssigner(func_spaces, W)
fc = df.Function(W)

# Current time step
rho_h = df.Function(Vr)
u_h = df.Function(Vu)
p_h = df.Function(Vp)

# Previous time step
rho_p = df.Function(Vr)
u_p = df.Function(Vu)

# Give names for output file
for func, name in [(rho_h, 'rho'), (rho_p, 'rho_p'),
                   (u_h, 'u'), (u_p, 'u_p'), (p_h, 'p')]:
    func.rename(name, name)

# Initial condition
el = Vr.ufl_element()
params = dict(rho0=inp.rho0, drho=inp.drho, Y0=inp.Y0, Ad=inp.Ad, Ld=inp.Ld,
              h=inp.height, b=inp.length, g=inp.g)
rho_expr = df.Expression(inp.rho_initial, element=el, **params)
rho_p.assign(df.interpolate(rho_expr, Vr))
rho_h.assign(rho_p)
rho_min = df.MPI.min(df.mpi_comm_world(), rho_h.vector().min())
rho_max = df.MPI.max(df.mpi_comm_world(), rho_h.vector().max())
df_rho_min = df.Constant(rho_min)
log.info('Density range: %.2f - %.2f\n' % (rho_min, rho_max))

# Slope limiters
if inp.slope_limiter_rho != 'none':
    slope_limiter_rho = SlopeLimiter(rho_h, inp.slope_limiter_rho)
    sle_rho = slope_limiter_rho.excedance
    sle_rho.rename('sleRho', 'Slope limiter exceedance for rho')

# Velocity projection
velproj = VelocityProjectionPrimal(u_h)

# Dolfin constants and functions
df_g = df.Constant(inp.g)
df_dt = df.Constant(inp.dt)
df_rho0 = df.Constant(inp.rho0)
df_mu = df.Constant(inp.nu)*rho_h
n = df.FacetNormal(mesh)
h = df.CellSize(mesh)
x = df.SpatialCoordinate(mesh)
zero = df.Constant(0.0)

# Tank motion
code_w0 = PyCode(inp.w0, 'input w0')
df_w0 = df.Constant(0.0)
gvec = df.as_vector([df_g*sin(df_w0), -df_g*cos(df_w0)])
def move_tank(t, it):
    df_w0.assign(code_w0.run(t=t, it=it, inp=inp))
move_tank(0.0, 0)

# Starting values
mass0 = df.assemble(rho_p*dx(mesh))
x_e = df.as_vector([x[0], x[1]+df.Constant(inp.height/2)])
Ek0, Ep0 = total_energy(rho_p, u_h, gvec, x_e)
log.info('Starting with mass %.15g\n' % mass0)
log.info('Starting with kinetic energy   %15.5e\n' % Ek0)
log.info('Starting with potential energy %15.5e\n' % Ep0)


###############################################################################
# Weak forms

rho, u1, u2, p = df.TrialFunctions(W)
vr, v1, v2, q = df.TestFunctions(W)
u = df.as_vector([u1, u2])
v = df.as_vector([v1, v2])

k_min, k_max = inp.nu*rho_min, inp.nu*rho_max
penalty_dS, penalty_ds = define_penalty(mesh, inp.order_u, k_min, k_max, log=log)
D11 = 0
D12 = df.as_vector([0, 0])

# Upwind and downwind velocities
u_conv = u_h
w_nU = (dot(u_conv, n) + abs(dot(u_conv, n)))/2.0
w_nD = (dot(u_conv, n) - abs(dot(u_conv, n)))/2.0

use_grad_q_form = True
use_grad_p_form = False
use_stress_divergence_form = False
c1, c2 = 1, -1

# Transport equation for rho
eq = (c1*rho + c2*rho_p)/df_dt*vr*dx
eq -= rho*dot(u_h, grad(vr))*dx
eq += jump(rho*w_nU)*jump(vr)*dS
#eq += rho/2*div(u_conv)*vr*dx # skew symmetric convection

sigma_h = df.sqr(rho_h)
sigma_p = df.sqr(rho_p)

# Momentum equations
for d in range(2):
    # Divergence free criterion
    # ∇⋅u = 0
    u_hat_p = avg(u[d])
    
    if use_grad_q_form:
        eq -= u[d]*q.dx(d)*dx
        eq += (u_hat_p + D12[d]*jump(u, n))*jump(q)*n[d]('+')*dS
    else:
        eq += q*u[d].dx(d)*dx
        eq -= (avg(q) - dot(D12, jump(q, n)))*jump(u[d])*n[d]('+')*dS
    
    # Time derivative
    # ∂(ρu)/∂t
    eq += sigma_h*(sigma_h*c1*u[d] + sigma_p*c2*u_p[d])/df_dt*v[d]*dx
    
    # Convection:
    # -w⋅∇(ρu)
    flux_nU = rho_h*u[d]*w_nU
    flux = jump(flux_nU)
    eq -= rho_h*u[d]*div(v[d]*u_conv)*dx
    eq += flux*jump(v[d])*dS
    
    # Skew symmetric convection
    eq += 1/2*div(rho_h*u_h)*u[d]*v[d]*dx
    
    # Diffusion:
    # -∇⋅∇u
    eq += df_mu*dot(grad(u[d]), grad(v[d]))*dx
    
    # Symmetric Interior Penalty method for -∇⋅μ∇u
    eq -= avg(df_mu)*dot(n('+'), avg(grad(u[d])))*jump(v[d])*dS
    eq -= avg(df_mu)*dot(n('+'), avg(grad(v[d])))*jump(u[d])*dS
    
    # Symmetric Interior Penalty coercivity term
    eq += penalty_dS*jump(u[d])*jump(v[d])*dS
    
    # -∇⋅μ(∇u)^T
    if use_stress_divergence_form:
        eq += df_mu*dot(u.dx(d), grad(v[d]))*dx
        eq -= avg(df_mu)*dot(n('+'), avg(u.dx(d)))*jump(v[d])*dS
        eq -= avg(df_mu)*dot(n('+'), avg(v.dx(d)))*jump(u[d])*dS
    
    # Pressure
    # ∇p
    if use_grad_p_form:
        eq += v[d]*p.dx(d)*dx
        eq -= (avg(v[d]) + D12[d]*jump(v, n))*jump(p)*n[d]('+')*dS
    else:
        eq -= p*v[d].dx(d)*dx
        eq += (avg(p) - dot(D12, jump(p, n)))*jump(v[d])*n[d]('+')*dS
    
    # Pressure continuity stabilization. Needed for equal order discretization
    if D11:
        eq += D11*dot(jump(p, n), jump(q, n))*dS
    
    # Body force (gravity)
    # ρ g
    eq -= rho_h*gvec[d]*v[d]*dx
    
    # Dirichlet boundary
    for dds, u_bc in ((ds, zero), ):
        # Divergence free criterion
        if use_grad_q_form:
            eq += q*u_bc*n[d]*dds
        else:
            eq -= q*u[d]*n[d]*dds
            eq += q*u_bc*n[d]*dds
        
        # Convection
        eq += rho_h*u[d]*w_nU*v[d]*dds
        eq += rho_h*u_bc*w_nD*v[d]*dds
        
        # SIPG for -∇⋅μ∇u
        eq -= df_mu*dot(n, grad(u[d]))*v[d]*dds
        eq -= df_mu*dot(n, grad(v[d]))*u[d]*dds
        eq += df_mu*dot(n, grad(v[d]))*u_bc*dds
        
        # Weak Dirichlet
        eq += penalty_ds*(u[d] - u_bc)*v[d]*dds
        
        # Pressure
        if not use_grad_p_form:
            eq += p*v[d]*n[d]*dds
    
    # Neumann boundary
    for nds, du_bc in ():
        # Divergence free criterion
        if use_grad_q_form:
            eq += q*u[d]*n[d]*nds
        else:
            eq -= q*u[d]*n[d]*nds
        
        # Convection
        eq += rho_h*u[d]*w_nU*v[d]*nds
        
        # Diffusion
        eq -= df_mu*du_bc*v[d]*nds
        
        # Pressure
        if not use_grad_p_form:
            eq += p*v[d]*n[d]*nds

a, L = df.system(eq)


###############################################################################
# Boundary conditions

all_boundaries = lambda x, on_boundary: on_boundary
#bcs = [df.DirichletBC(W.sub(1), zero, all_boundaries),
#       df.DirichletBC(W.sub(2), zero, all_boundaries)]
bcs = []


###############################################################################
# Linear equation solvers

solver = df.PETScLUSolver('petsc')

###############################################################################
# Run simulation

# Calculate solution properties
solprops = SolutionProperties(mesh, u_h, df_dt, inp.nu, divergence='div')
solprops._div_dS.rename('div_dS', 'div_dS')
solprops._div_dx.rename('div_dx', 'div_dx')

# Prepare XDMF output file to store results for visualisation
xdmf_funcs = [rho_h, u_h, p_h, solprops._div_dS, solprops._div_dx]
if inp.slope_limiter_rho != 'none':
    xdmf_funcs.append(slope_limiter_rho.excedance)
xdmf = XDMFSaver(xdmf_funcs, 'results')

# Functions needed to restart
restartfuncs = [rho_p, u_p, p_h]

# Create null space vector in Vp Space
null_func = df.Function(Vp)
null_vec = null_func.vector()
null_vec[:] = 1
null_vec *= 1/null_vec.norm("l2")

# Convert null space vector to coupled space
null_func2 = df.Function(W)
fa = df.FunctionAssigner(W.sub(3), Vp)
fa.assign(null_func2.sub(3), null_func)
pressure_null_space = df.VectorSpaceBasis([null_func2.vector()])

def time_loop():
    t = 0.0
    it = 0
    xdmf.save(t)
    
    while t <= inp.tmax - inp.dt + 1e-6:
        it += 1
        t += inp.dt
        t0_all = time.time()
        
        # Restart from file
        if it == 1 and inp.restart_from_file:
            log.info('Loading restart file %r\n' % inp.restart_from_file)
            t, it = load_restart_file(inp.restart_from_file, restartfuncs, log)
        
        log.report('Time', t, '%7.3f')
        move_tank(t, it)
        
        with log.timer('assemble'):
            A, b = df.assemble_system(a, L, bcs)
            
            # Make sure the null space is set on the matrix
            df.as_backend_type(A).set_nullspace(pressure_null_space)

            # Orthogonalize b with respect to the null space
            pressure_null_space.orthogonalize(b)
        
        with log.timer('solve'):
            solver.solve(A, fc.vector(), b)
            fa_split.assign([rho_h, u_h.sub(0), u_h.sub(1), p_h], fc)
            for f in [rho_h, u_h, p_h]:
                f.vector().apply('insert') # dolfin bug #587
            
            if inp.slope_limiter_rho != 'none':
                slope_limiter_rho.run()
                
            velproj.run()
            u_h.assign(velproj.velocity)
            
            # Removing the null space of the matrix system is not strictly the same as removing
            # the null space of the equation, so we correct for this here
            vol = df.assemble(df.Constant(1)*dx(domain=mesh))
            p_avg = df.assemble(p_h*dx(domain=mesh))/vol
            p_h.vector()[:] -= p_avg
        
        # Advance in time
        rho_p.assign(rho_h)
        u_p.assign(u_h)
        
        with log.timer('solprops'):
            # Calculate mass errors
            mass = df.assemble(rho_h*dx)
            
            # Calculate the Courant number
            Co_max = solprops.courant_number().vector().max()
            
            # Calculate the divergence
            div_dS_f, div_dx_f = solprops.divergences()
            div_dS = div_dS_f.vector().max()
            div_dx = div_dx_f.vector().max()
            
            # Calculate the energy
            Ek, Ep = total_energy(rho_p, u_h, gvec, x_e)
            
        # Save visualization file
        with log.timer('save'):
            if it % inp.output_step == 0:
                xdmf.save(t)
        
        log.report('TsTime', time.time() - t0_all, '%6.2fs')
        log.report('mass', (mass - mass0), '% 8.4e')
        log.report('min(rho)', rho_h.vector().min(), '%6.3f')
        log.report('max(rho)', rho_h.vector().max(), '%6.3f')
        #log.report('div_dS', div_dS, '%6.3e')
        #log.report('div_dx', div_dx, '%6.3e')
        log.report('div', div_dx+div_dS, '%6.1e')
        log.report('Ek', Ek-Ek0, '%6.2e')
        log.report('Ep', Ep-Ep0, '%6.2e')
        log.report('Co', Co_max, '%6.1e')
        log.report_timestep()
        
        if should_stop():
            log.warning('Stop requested\n')
            break
    
    return t, it


t0_sim = time.time()
try:
    t, it = time_loop()
    ok = True
except:
    # Log the error
    error = traceback.format_exc()
    log.error(error)
    ok = False


mass = df.assemble(rho_h*dx)
log.info('Simulation done in %5.3fs on %d CPUs\n' % (time.time() - t0_sim, NCPUS))
log.info('Ending with mass %.15g\n' % mass)
if ok:
    log.info('Saving to restart file %r\n' % inp.restart_file) 
    save_restart_file(inp.restart_file, inp, log, mesh, restartfuncs, t, it)
log.info('Simulation status: %s\n' % ('SUCCESS' if ok else 'FAILURE'))

if '--plot' in sys.argv:
    from matplotlib import pyplot
    fig = pyplot.figure()
    ax = fig.add_subplot(111, title='Rho')
    df.plot(rho_h, backend='matplotlib')
    pyplot.show()

sys.exit(0 if ok else 1)
