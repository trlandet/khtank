# encoding: utf8
from __future__ import division
import time, traceback, sys
import dolfin as df
from dolfin import grad, div, dot, inner, dx, ds, dS, cos, sin, jump
from kh_tank_helpers import SlopeLimiter, SolutionProperties, Input, total_energy
from orangutan import XDMFSaver, Log, should_stop, save_restart_file, load_restart_file, NCPUS, PyCode


###############################################################################
# Input data

inpfile = sys.argv[1]
print 'Reading input file %s' % inpfile
inp = Input()
Input.space_rho = 'DG'
Input.order_rho = 1
Input.space_u = 'CG'
Input.order_u = 2
Input.space_p = 'CG'
Input.order_p = 1
Input.p_initial = '0'
inp.read(inpfile)


###############################################################################
# Preliminaries

# Setup logging
log = Log(inp.output_file)
df.set_log_level(df.WARNING)
log.info('Running KH Tank with input:\n')
log.info(str(inp))
log.info('---------\n')

# Mesh and function spaces
df.parameters['ghost_mode'] = 'shared_vertex'
p0 = df.Point(-inp.length/2, -inp.height/2)
p1 = df.Point(inp.length/2, inp.height/2)
mesh = df.RectangleMesh(p0, p1, inp.Nx, inp.Ny, inp.diagonal)
Vr = df.FunctionSpace(mesh, inp.space_rho, inp.order_rho)
Vu = df.VectorFunctionSpace(mesh, inp.space_u, inp.order_u)
Vp = df.FunctionSpace(mesh, inp.space_p, inp.order_p)

# Current time step
rho_h = df.Function(Vr)
u_h = df.Function(Vu)
p_h = df.Function(Vp)
phi_h = df.Function(Vp)

# Previous time step
rho_p = df.Function(Vr)
u_p = df.Function(Vu)

# Give names for output file
for func, name in [(rho_h, 'rho'), (rho_p, 'rho_p'),
                   (u_h, 'u'), (u_p, 'u_p'),
                   (p_h, 'p'), (phi_h, 'phi')]:
    func.rename(name, name)

# Initial condition
el = Vr.ufl_element()
params = dict(rho0=inp.rho0, drho=inp.drho, Y0=inp.Y0, Ad=inp.Ad, Ld=inp.Ld,
              h=inp.height, b=inp.length, g=inp.g)
rho_expr = df.Expression(inp.rho_initial, element=el, **params)
rho_p.assign(df.interpolate(rho_expr, Vr))
rho_h.assign(rho_p)
rho_min = df.MPI.min(df.mpi_comm_world(), rho_h.vector().min())
df_rho_min = df.Constant(rho_min)
p_expr = df.Expression(inp.p_initial, element=el, **params)
p_h.assign(df.interpolate(p_expr, Vp))

# Slope limiters
if inp.slope_limiter_rho != 'none':
    slope_limiter_rho = SlopeLimiter(rho_h, inp.slope_limiter_rho)
    sle_rho = slope_limiter_rho.excedance
    sle_rho.rename('sleRho', 'Slope limiter exceedance for rho')

# Dolfin constants and functions
df_g = df.Constant(inp.g)
df_dt = df.Constant(inp.dt)
df_rho0 = df.Constant(inp.rho0)
df_mu = df.Constant(inp.nu)*rho_h
n = df.FacetNormal(mesh)
h = df.CellSize(mesh)
x = df.SpatialCoordinate(mesh)
zero = df.Constant(0.0)
zerovec = df.Constant([0.0, 0.0])

# Tank motion
code_w0 = PyCode(inp.w0, 'input w0')
df_w0 = df.Constant(0.0)
gvec = df.as_vector([df_g*sin(df_w0), -df_g*cos(df_w0)])
def move_tank(t, it):
    df_w0.assign(code_w0.run(t=t, it=it, inp=inp))
move_tank(0.0, 0)

# Starting values
mass0 = df.assemble(rho_p*dx(mesh))
x_e = df.as_vector([x[0], x[1]+df.Constant(inp.height/2)])
Ek0, Ep0 = total_energy(rho_p, u_h, gvec, x_e)
log.info('Starting with mass %.15g\n' % mass0)
log.info('Starting with kinetic energy   %15.5e\n' % Ek0)
log.info('Starting with potential energy %15.5e\n' % Ep0)


###############################################################################
# Weak forms

rho = df.TrialFunction(Vr)
u = df.TrialFunction(Vu)
phi = df.TrialFunction(Vp)
vr = df.TestFunction(Vr)
vu = df.TestFunction(Vu)
vp = df.TestFunction(Vp)

# Upwind velocity
upw = (abs(dot(u_h, n)) + dot(u_h, n))/2

# Equation 1 - transport equation for rho
eq1 = (rho - rho_p)/df_dt*vr*dx
eq1 -= rho*dot(u_h, grad(vr))*dx
eq1 += jump(rho*upw)*jump(vr)*dS
a1, L1 = df.system(eq1)

# Equation 2 - momentum prediction
eq2 = 1/df_dt*dot((rho_h + rho_p)/2*u - rho_p*u_p, vu)*dx
eq2 += rho_h*dot(dot(grad(u), u_p), vu)*dx
eq2 += 1/2*div(rho_h*u_h)*dot(u, vu)*dx
eq2 += df_mu*inner(grad(u), grad(vu))*dx
eq2 += dot(grad(p_h + phi_h), vu)*dx
eq2 -= rho_h*dot(gvec, vu)*dx
a2, L2 = df.system(eq2)

# Equation 3 - pressure update
eq3 = dot(grad(phi), grad(vp))*dx
eq3 -= df_rho_min/df_dt*div(u_h)*vp*dx
a3, L3 = df.system(eq3)


###############################################################################
# Boundary conditions

all_boundaries = lambda x, on_boundary: on_boundary
bcs2 = [df.DirichletBC(Vu, zerovec, all_boundaries)]


###############################################################################
# Linear equation solvers

solver1 = df.KrylovSolver('gmres', 'default')
solver2 = df.KrylovSolver('gmres', 'default')
solver3 = df.KrylovSolver('gmres', 'hypre_amg')

for solver in (solver1, solver2, solver3):
    solver.parameters['nonzero_initial_guess'] = True
    solver.parameters['relative_tolerance'] = 1e-15
    solver.parameters['absolute_tolerance'] = 1e-15
    solver.parameters['preconditioner']['structure'] = 'same_nonzero_pattern'
solver3.parameters['preconditioner']['structure'] = 'same'


###############################################################################
# Run simulation

# Prepare XDMF output file to store results for visualisation
xdmf_funcs = [rho_h, u_h, p_h, phi_h]
if inp.slope_limiter_rho != 'none':
    xdmf_funcs.append(slope_limiter_rho.excedance)
xdmf = XDMFSaver(xdmf_funcs, 'results')

# Calculate solution properties
solprops = SolutionProperties(mesh, u_h, df_dt, inp.nu, divergence='gradq_avg')

# Functions needed to restart
restartfuncs = [rho_p, u_p, p_h, phi_h]

# Pre assemble Poisson matrix and setup null-space
A3 = df.assemble(a3)
null_vec = df.Vector(p_h.vector())
null_vec[:] = 1
null_vec *= 1/null_vec.norm("l2")
pressure_null_space = df.VectorSpaceBasis([null_vec])
df.as_backend_type(A3).set_nullspace(pressure_null_space)

def time_loop():
    t = 0.0
    it = 0
    xdmf.save(t)
    
    while t <= inp.tmax - inp.dt + 1e-6:
        it += 1
        t += inp.dt
        t0_all = time.time()
        
        # Restart from file
        if it == 1 and inp.restart_from_file:
            log.info('Loading restart file %r\n' % inp.restart_from_file)
            t, it = load_restart_file(inp.restart_from_file, restartfuncs, log)
        
        log.report('Time', t, '%7.3f')
        move_tank(t, it)
        
        # Linear solve 1 - find rho
        with log.timer('rho'):
            A1, b1 = df.assemble_system(a1, L1)
            solver1.solve(A1, rho_h.vector(), b1)
            if inp.slope_limiter_rho != 'none':
                slope_limiter_rho.run()
        
        # Linear solve 2 - find u
        with log.timer('u'):
            A2, b2 = df.assemble_system(a2, L2, bcs2)
            solver2.solve(A2, u_h.vector(), b2)
        
        # Linear solve 3 - find phi
        with log.timer('phi'):
            b3 = df.assemble(L3)
            pressure_null_space.orthogonalize(b3)
            solver3.solve(A3, phi_h.vector(), b3)
            
            # Removing the null space of the matrix system is not strictly the same as removing
            # the null space of the equation, so we correct for this here
            vol = df.assemble(df.Constant(1)*dx(domain=mesh))
            phi_avg = df.assemble(phi_h*dx(domain=mesh))/vol
            phi_h.vector()[:] -= phi_avg
        
        # Advance in time
        rho_p.assign(rho_h)
        u_p.assign(u_h)
        p_h.vector().axpy(1, phi_h.vector())
        
        with log.timer('solprops'):
            # Calculate mass errors
            mass = df.assemble(rho_h*dx)
            
            # Calculate the Courant number
            Co_max = solprops.courant_number().vector().max()
            
            # Calculate the divergence
            div_dS_f, div_dx_f = solprops.divergences()
            div_dS = div_dS_f.vector().max()
            div_dx = div_dx_f.vector().max()
            
            # Calculate the energy
            Ek, Ep = total_energy(rho_p, u_h, gvec, x_e)
            
        # Save visualization file
        with log.timer('save'):
            if it % inp.output_step == 0:
                xdmf.save(t)
        
        log.report('TsTime', time.time() - t0_all, '%6.2fs')
        log.report('mass', (mass - mass0), '% 8.4e')
        log.report('min(rho)', rho_h.vector().min(), '%6.3f')
        log.report('max(rho)', rho_h.vector().max(), '%6.3f')
        #log.report('div_dS', div_dS, '%6.3e')
        #log.report('div_dx', div_dx, '%6.3e')
        log.report('div', div_dx+div_dS, '%6.1e')
        log.report('Ek', Ek-Ek0, '%6.2e')
        log.report('Ep', Ep-Ep0, '%6.2e')
        log.report('Co', Co_max, '%6.1e')
        log.report_timestep()
        
        if should_stop():
            log.warning('Stop requested\n')
            break
    
    return t, it


t0_sim = time.time()
try:
    t, it = time_loop()
    ok = True
except:
    # Log the error
    error = traceback.format_exc()
    log.error(error)
    ok = False


mass = df.assemble(rho_h*dx)
log.info('Simulation done in %5.3fs on %d CPUs\n' % (time.time() - t0_sim, NCPUS))
log.info('Ending with mass %.15g\n' % mass)
if ok:
    log.info('Saving to restart file %r\n' % inp.restart_file) 
    save_restart_file(inp.restart_file, inp, log, mesh, restartfuncs, t, it)
log.info('Simulation status: %s\n' % ('SUCCESS' if ok else 'FAILURE'))

if '--plot' in sys.argv:
    from matplotlib import pyplot
    fig = pyplot.figure()
    ax = fig.add_subplot(111, title='Rho')
    df.plot(rho_h, backend='matplotlib')
    pyplot.show()

sys.exit(0 if ok else 1)
