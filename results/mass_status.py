import os, sys

resdir = sys.argv[1] if len(sys.argv) > 1 else '.'

logs = []
for dirname, dirs, files in os.walk(resdir):
    for filename in files:
        if filename.endswith('log'):
            logs.append(os.path.join(dirname, filename))
prefix = os.path.commonprefix(logs)
postfix = os.path.commonprefix([fn[::-1] for fn in logs])[::-1]

errorplots = []
for logfilename in sorted(logs):
    info = startmass = startrhomin = startrhomax = None
    t = []
    err = []
    for line in open(logfilename, 'rt'):
        if 'Co:' in line:
            wds = line.split()
            labels = [w[:-1] for w in wds[::2]]
            values = [(float(w) if not w.endswith('s') else float(w[:-1])) for w in wds[1::2]]
            info = {l: v for l, v in zip(labels, values)}
            rhomin, rhomax = info['min(rho)'], info['max(rho)']
            if startrhomin is None:
                startrhomin, startrhomax = rhomin, rhomax
            t.append(info['Time'])
            e1 = abs(info['mass'])/startmass
            dr = startrhomax - startrhomin
            e2 = abs(rhomin - startrhomin)/dr if rhomin < startrhomin else 0
            e3 = abs(rhomax - startrhomax)/dr if rhomax > startrhomax else 0
            err.append(e1 + e2 + e3)
        elif line.startswith('Starting with mass'):
            startmass = float(line.split()[3])

    if not err:
        print logfilename, 'INCOMPLETE'
        continue    
    print '%.2f %s %.2e' % (info['Time'], logfilename, err[-1])
    print '\t % .2e % .2e % .2e' % (info['mass'], info['min(rho)'], info['max(rho)'])
    print '\t % .2e % .2e % .2e' % (e1, e2, e3)

    errorplots.append((logfilename[len(prefix):-len(postfix)], t, err))

print '-'*80
from matplotlib import pyplot
pyplot.style.use('ggplot')

fig = pyplot.figure(figsize=(8,6))
ax = fig.add_subplot(111)
ax.set_title('Effect of slope limiter')
for label, t, err in errorplots:
    if not label:
        label = 'NOLABEL'
    print label
    conf = dict(label=label,
                ls = ':' if 'velproj_simple' in label else '-',
                lw=2)
    ax.loglog(t, err, **conf)
ax.legend(loc='lower right')
ax.set_xlabel('Time [s]')
ax.set_ylabel('Relative mass error')
fig.tight_layout()
pyplot.show()
