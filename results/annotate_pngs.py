# encoding: utf8
from __future__ import division
import sys, os, io
from PIL import Image, ImageFont, ImageDraw
import numpy
import h5py

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot

NAMES = ['Basic limiter', 'No limiter']

if False:
    TOP_H5 = 'microkickstart_lim/restart.h5'
    BOT_H5 = 'microkickstart_nolim/restart.h5'
    PNGS = 'microkick_compare'
    OUTPUT_STEP = 10
    w0_func = lambda t: 0.01*numpy.sin(t)*numpy.exp(-t)
else:
    TOP_H5 = 'fulltilt_lim/restart.h5'
    BOT_H5 = 'fulltilt_nolim/restart.h5'
    PNGS = 'fulltilt_compare'
    OUTPUT_STEP = 25
    w0_func = None

X = [10, 888] # Text location in pixels from left
Y = [14, 121, 248, 356] # Text location in pixels from top
OUTDIR = os.path.join(PNGS, 'postprocessed')
FONT_TTF = '/usr/share/fonts/overpass/Overpass_Regular.ttf'
FONT_TTF_MONO = '/usr/share/fonts/liberation/LiberationMono-Regular.ttf'
FONT_SIZE = 14

##################################################################

# Make/clean the output directory
if os.path.exists(OUTDIR):
    for fn in os.listdir(OUTDIR):
        os.unlink(os.path.join(OUTDIR, fn))
else:
    os.mkdir(OUTDIR)
    
font = ImageFont.truetype(FONT_TTF, FONT_SIZE+4)
font_mono = ImageFont.truetype(FONT_TTF_MONO, FONT_SIZE)
fnt = dict(font=font, fill=(255, 255, 255, 255))
fnt_mono = dict(font=font_mono, fill=(255, 255, 255, 255))

def get_time_series(h5_file_name):
    h5 = h5py.File(h5_file_name, 'r')
    ts = {}
    reps = h5['/reports']
    for name in reps:
        ts[name] = numpy.array(reps[name])
    return ts

top_ts = get_time_series(TOP_H5)
bot_ts = get_time_series(BOT_H5)
pngs = sorted([os.path.join(PNGS, fn) for fn in os.listdir(PNGS) if fn.endswith('.png')])

N1 = len(top_ts['Time'])
N2 = len(bot_ts['Time'])
N3 = len(pngs)

print N1, N2, N3
assert N1 == N2 == (N3-1)*OUTPUT_STEP
dY = Y[1]-Y[0]

for i in range(N3-1):
    sys.stdout.write('.')
    png = pngs[i+1]
    ii = i*OUTPUT_STEP
    
    im = Image.open(png)
    dr = ImageDraw.Draw(im)
    
    dr.text((X[0], Y[0]+10), NAMES[0], **fnt)
    dr.text((X[0], Y[2]+10), NAMES[1], **fnt)
    
    dr.text((X[1], Y[1]+dY-50), 'rho-min = %6.1f' % top_ts['min(rho)'][ii], **fnt_mono)
    dr.text((X[1], Y[1]+dY-30), 'rho-max = %6.1f' % top_ts['max(rho)'][ii], **fnt_mono)
    dr.text((X[1], Y[3]+dY-50), 'rho-min = %6.1f' % bot_ts['min(rho)'][ii], **fnt_mono)
    dr.text((X[1], Y[3]+dY-30), 'rho-max = %6.1f' % bot_ts['max(rho)'][ii], **fnt_mono)
    
    if w0_func:
        buf = io.BytesIO()
        fig = pyplot.figure(figsize=(10, 1.9))
        t = top_ts['Time']
        y = w0_func(t)
        ax = fig.add_axes([0.01, 0.01, .98, .98])
        ax.plot(t, y, c='white', lw=2)
        ax.plot(t[ii], y[ii], marker='o', markerfacecolor='white', markersize=10)
        
        I = numpy.argmax(abs(y))
        ax.text(t[I]+2, y[I]*0.9, '%.2f deg' % (y[I]*180/numpy.pi), color='white', size=16)
        ax.text(t[-1]*0.9, y[-1] + y[I]*0.1, '%.2f deg' % (y[-1]*180/numpy.pi), color='white', size=16)
        ax.text(t[-1]*0.5, y[I]*0.5, 'Tilt angle', color='white', size=18)
        
        ax.axis('off')
        ax.set_xlim(-1, t[-1]+1)
        fig.savefig(buf, format='png', transparent=True, dpi=50)
        pyplot.close()
        buf.seek(0)
        
        im2 = Image.open(buf)
        im.paste(im2, (0, Y[3]), mask=im2)
    
    # Make sure dimensions are multiples of 2
    w, h = im.size
    if w % 2: w -= 1
    if h % 2: h -= 1
    if (w, h) != im.size:
        im = im.crop((0, 0, w, h))

    im.save(os.path.join(OUTDIR, os.path.split(png)[1]))
    #im.show(); break

print '\nDone with %d images' % (N3-1)
print 'To make video:'
print 'ffmpeg -f image2 -r 7 -pattern_type glob -i \'{0}/*.png\' -c:v libx264 -pix_fmt yuv420p -y {0}/movie.mp4'.format(OUTDIR)
