# enconding: utf8
from __future__ import division
import sys
import numpy
from matplotlib import pyplot
import h5py


h5_file_names = sys.argv[1:]

fig = pyplot.figure()
axCo = fig.add_subplot(111, title='Co')

fig = pyplot.figure()
axEt = fig.add_subplot(111, title='Et')

fig = pyplot.figure()
axM = fig.add_subplot(111, title='M')

for h5_file_name in h5_file_names:
    h5 = h5py.File(h5_file_name, 'r')
    dt = h5['/metadata/time_info'][2]
     
    reps = h5['/reports']
    t = numpy.array(reps['Time'])
    Ek = numpy.array(reps['Ek'])
    Ep = numpy.array(reps['Ep'])
    Co = numpy.array(reps['Co'])
    mass = numpy.array(reps['mass'])
    
    
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    ax.set_title('E '+h5_file_name)
    ax.plot(t, Ek, label='Ek')
    ax.plot(t, Ep, label='Ep')
    ax.plot(t, Ek+Ep, label='E')
    ax.legend(loc='best')
    
    axCo.plot(t, Co, label=h5_file_name)
    axEt.plot(t, Ek+Ep, label=h5_file_name)
    axM.plot(t, mass, label=h5_file_name)

axCo.legend(loc='best')
axEt.legend(loc='best')
axM.legend(loc='best')

pyplot.show()
